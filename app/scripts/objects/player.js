export default class Player extends Phaser.GameObjects.Sprite {
  /**
   *  My custom sprite.
   *
   *  @constructor
   *  @class Player
   *  @extends Phaser.GameObjects.Sprite
   *  @param {Phaser.Scene} scene - The scene that owns this sprite.
   *  @param {number} x - The horizontal coordinate relative to the scene viewport.
   *  @param {number} y - The vertical coordinate relative to the scene viewport.
   */
  constructor(scene, x, y) {
    super(scene, x, y, 'player');

    //  Add this game object to the owner scene.
    this.scene = scene;
    this.scene.add.existing(this);
    this.scene.physics.world.enable(this);
    this.body.collideWorldBounds = true;
    this.body.setSize(12, 15);
    this.body.offset.x = 7;
    this.body.offset.y = 5;
    this.anims.play('gabe-idle');
    this.keys = this.scene.input.keyboard.addKeys({
      left: Phaser.Input.Keyboard.KeyCodes['LEFT'],
      right: Phaser.Input.Keyboard.KeyCodes['RIGHT'],
      up: Phaser.Input.Keyboard.KeyCodes['UP'],
      down: Phaser.Input.Keyboard.KeyCodes['DOWN'],
      fire: Phaser.Input.Keyboard.KeyCodes['C'],
    }); 
    this.moving = false;
    this.action = false;
  }
  preUpdate(t, dt){
    super.preUpdate(t, dt);
    const speed = 100;

    this.body.setVelocity(0);
    
    if(this.keys.left.isDown){
      this.setFlip(true);
      this.body.setVelocityX(-speed);
    }else if(this.keys.right.isDown){
      this.setFlip(false);
      this.body.setVelocityX(speed);
    }else if(this.keys.up.isDown){
      this.body.setVelocityY(-speed);
    }else if(this.keys.down.isDown){
      this.body.setVelocityY(speed);
    }

    this.body.velocity.normalize().scale(speed);

    if(this.keys.left.isDown || this.keys.right.isDown || this.keys.up.isDown || this.keys.down.isDown) {
      this.anims.play('gabe-run', true);
    }else {
      this.anims.play('gabe-idle');
    }


    if(Phaser.Input.Keyboard.JustDown(this.keys.fire)){
      this.action = true;
    }else{
      this.action = false;
    }
  }

  freeze(){
    this.body.moves = false;
  }
}
