export default class Sensei extends Phaser.GameObjects.Sprite {
  /**
   *  My custom sprite.
   *
   *  @constructor
   *  @class Sensei
   *  @extends Phaser.GameObjects.Sprite
   *  @param {Phaser.Scene} scene - The scene that owns this sprite.
   *  @param {number} x - The horizontal coordinate relative to the scene viewport.
   *  @param {number} y - The vertical coordinate relative to the scene viewport.
   */
  constructor(scene, x, y) {
    super(scene, x, y, 'sensei');

    //  Add this game object to the owner scene.
    this.scene.add.existing(this);
    this.scene.physics.world.enable(this);
    this.scene.add.image('sensei');
    this.body.moves = false;

    this.haveConversation = this.scene.add.image(this.x + 12, this.y - 8, 'talk-bubble').setDepth(12);
    this.haveConversation.alpha = 0;
    this.scene.tweens.add({
      targets: this.haveConversation,
      y : this.y-10,
      duration: 200,
      ease: "Power2",
      yoyo: true,
      loop: -1
    });
  }

  preUpdate(d, dt){
    super.preUpdate(d, dt);

    if(this.active){
      const player = this.scene.player;
      if(Phaser.Math.Distance.Between(player.x, player.y, this.x, this.y) <= 30){
        this.haveConversation.alpha = 1;
      }else{
        this.haveConversation.alpha = 0;
      }
    }
  }

  talk(){
    return "It's dangerous \nto go alone! Take this!";
  }
}
