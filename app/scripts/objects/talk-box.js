export default class TalkBox extends Phaser.GameObjects.Image {
  /**
   *  My custom image.
   *
   *  @constructor
   *  @class TalkBox
   *  @extends Phaser.GameObjects.Image
   *  @param {Phaser.Scene} scene - The scene that owns this image.
   *  @param {number} x - The horizontal coordinate relative to the scene viewport.
   *  @param {number} y - The vertical coordinate relative to the scene viewport.
   */
  constructor(scene, x, y, config) {
    super(scene, x, y, 'talk-box', config);
    this.scene = scene;
    this.textBox = this.scene.add.image(x, y, 'text-box').setDepth(11).setScale(1.3);
    this.charTalking = this.scene.add.image(x - 50, y - 4, `${config.char}-face`).setDepth(11).setScale(1.9);
    this.text = this.scene.add.text(x - 28, y - 20, `${config.char.toUpperCase()} :`, {
      font: 'bold 10px Arial',
      color: '#000',
      weight: "bold"
    }).setDepth(11);

    this.say = this.scene.add.text(x - 27, y - 10, `${config.text} :`, {
      font: '9px Arial',
      color: '#000',
      weight: "bold"
    }).setDepth(11);

    this.displayNone();
  }
  displayNone() {
    this.textBox.alpha = 0;
    this.charTalking.alpha = 0;
    this.text.alpha = 0;
    this.say.alpha = 0;
  }
  displayShow() {
    this.textBox.alpha = 1;
    this.charTalking.alpha = 1;
    this.text.alpha = 1;
    this.say.alpha = 1;
  }
}
