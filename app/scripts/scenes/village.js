import Player from '@/objects/player';
import Sensei from '@/objects/sensei';
import TalkBox from '@/objects/talk-box';

export default class Village extends Phaser.Scene {
  /**
   *  My custom scene.
   *
   *  @extends Phaser.Scene
   */
  constructor() {
    super({key: 'Village'});
  }

  /**
   *  Called when this scene is initialized.
   *
   *  @protected
   *  @param {object} [data={}] - Initialization parameters.
   */
  init(/* data */) {
  }

  /**
   *  Used to declare game assets to be loaded using the loader plugin API.
   *
   *  @protected
   */
  preload() {
  }

  /**
   *  Responsible for setting up game objects on the screen.
   *
   *  @protected
   *  @param {object} [data={}] - Initialization parameters.
   */
  create(/* data */) {
    let map = this.make.tilemap({key: 'village', tileWidth: 16,tileHeight: 16});
    let tiles = map.addTilesetImage('atlas','tiles');
    
    this.music = this.sound.add('red-carpet', {volume: 0.02, loop: true});
    this.music.play();

    this.groundLayer = map.createDynamicLayer('Ground', tiles, 0, 0);
    this.roadLayer = map.createDynamicLayer('Road', tiles, 0, 0);
    this.riverLacLayer = map.createDynamicLayer('River&Lac', tiles, 0, 0);
    this.grassFlowerLayer = map.createDynamicLayer('Grass&Flower', tiles, 0, 0);
    this.fenceLayer = map.createDynamicLayer('Fence', tiles, 0, 0).setDepth(10);
    this.bridgeLayer = map.createDynamicLayer('Bridge', tiles, 0, 0);
    this.rocLayer = map.createDynamicLayer('Roc', tiles, 0, 0);
    this.treeLayer = map.createDynamicLayer('Tree', tiles, 0, 0).setDepth(10);
    this.houseLayer = map.createDynamicLayer('House', tiles, 0, 0).setDepth(10);
    this.objectLayer = map.createDynamicLayer('Object', tiles, 0, 0).setDepth(10);

    this.player = new Player(this, 10, 175);
    this.player.setDepth(9);

    this.sensei = new Sensei(this, 145, 80);
    this.sensei.setDepth(11);

    this.riverLacLayer.setCollisionByProperty({collide: true});
    this.fenceLayer.setCollisionByProperty({collide: true});
    this.bridgeLayer.setCollisionByProperty({collide: true});
    this.rocLayer.setCollisionByProperty({collide: true});
    this.treeLayer.setCollisionByProperty({collide: true});
    this.houseLayer.setCollisionByProperty({collide: true});
    this.objectLayer.setCollisionByProperty({collide: true});

    this.physics.add.collider(this.player, [this.fenceLayer, this.riverLacLayer ,this.bridgeLayer ,this.rocLayer ,this.treeLayer ,this.houseLayer, this.objectLayer, this.sensei]);

    this.talking = false;
    
    this.physics.add.overlap(this.player, this.sensei, (player, npc) => {
      if(player.action){
        if(!this.talking){
          this.talking = true;
          this.textBox = new TalkBox(this, npc.x + 30, npc.y + 40, {char : "sensei", text : npc.talk()});
          this.textBox.displayShow();
        }
        if(!player.body.moves && this.talking){
          this.talking = false;
          this.textBox.displayNone();
          player.body.moves = true;
        }
      }else if(this.talking && player.body.moves){
        player.freeze();
      }
     }, null, this);
  }

  /**
   *  Handles updates to game logic, physics and game objects.
   *
   *  @protected
   *  @param {number} t - Current internal clock time.
   *  @param {number} dt - Time elapsed since last update.
   */
  update(/* t, dt */) {
  }

  /**
   *  Called after a scene is rendered. Handles rendenring post processing.
   *
   *  @protected
   */
  render() {
  }

  /**
   *  Called when a scene is about to shut down.
   *
   *  @protected
   */
  shutdown() {
  }

  /**
   *  Called when a scene is about to be destroyed (i.e.: removed from scene
   *  manager). All allocated resources that need clean up should be freed up
   *  here.
   *
   *  @protected
   */
  destroy() {
  }
}
