import * as config from '@/config';

export default class SplashScreen extends Phaser.Scene {
  /**
   *  Takes care of loading the main game assets, including textures, tile
   *  maps, sound effects and other binary files, while displaying a busy
   *  splash screen.
   *
   *  @extends Phaser.Scene
   */
  constructor() {
    super({
      key: 'SplashScreen',

      //  Splash screen and progress bar textures.
      pack: {
        files: [{
          key: 'splash-screen',
          type: 'image'
        }, {
          key: 'progress-bar',
          type: 'image'
        }]
      }
    });
  }

  /**
   *  Show the splash screen and prepare to load game assets.
   *
   *  @protected
   */
  preload() {
    //  Display cover and progress bar textures.
    this.showCover();
    this.showProgressBar();

    //  HINT: Declare all game assets to be loaded here.
    this.load.image('tiles', 'map/atlas.png');
    this.load.tilemapTiledJSON('village', 'map/village.json');
    this.load.spritesheet('gabe', 'chars/gabe/gabe-idle-run.png', {
      frameHeight: 24,
      frameWidth: 24
    });
    this.load.image('sensei', 'chars/sensei/sensei.png');
    this.load.image('sensei-face', 'chars/sensei/sensei_face.png');
    this.load.image('talk-bubble', 'UI/generic-rpg-ui-inventario03.png');
    this.load.image('text-box', 'UI/generic-rpg-ui-text-box.png');

    this.load.image('splash-screen', 'village.png');
    
    this.load.image('game-title', 'game-title.png');
    this.load.image('title-screen', 'title-screen.png');
    this.load.spritesheet('start-btn', 'UI/start-btn.png', {
      frameWidth: 102,
      frameHeight: 44
    });

    this.load.audio('red-carpet', ['soundtrack/Red_Carpet_Wooden_Floor.mp3']);
    this.load.audio('celestial', ['soundtrack/Celestial.mp3']);
    this.load.audio('bonus', ['soundtrack/danse_canards_8bit.mp3']);

  }

  /**
   *  Set up animations, plugins etc. that depend on the game assets we just
   *  loaded.
   *
   *  @protected
   */
  create() {
    //  We have nothing left to do here. Start the next scene.
    this.scene.start('TitleScreen');
    this.anims.create({
      key: 'gabe-run',
      frames: this.anims.generateFrameNumbers('gabe', {start: 1, end: 6}),
      frameRate: 8,
      repeat: -1
    });
    this.anims.create({
      key: 'gabe-idle',
      frames: this.anims.generateFrameNumbers('gabe', {start: 0, end: 0}),
      frameRate: 8,
      repeat: -1
    });

  }

  //  ------------------------------------------------------------------------

  /**
   *  Show the splash screen cover.
   *
   *  @private
   */
  showCover() {
    let sp =this.add.image(config.width/2, config.height/2, 'splash-screen').setScale(.5);
    sp.alpha = .8;
  }

  /**
   *  Show the progress bar and set up its animation effect.
   *
   *  @private
   */
  showProgressBar() {
    //  Get the progress bar filler texture dimensions.
    const {width: w, height: h} = this.textures.get('progress-bar').get();

    //  Place the filler over the progress bar of the splash screen.
    const img = this.add.sprite(0, config.height - 100, 'progress-bar').setOrigin(0);
    img.setScale(.5);
    this.add.text(config.width/2 - 40, config.height - 130, 'LOADING...', {
      weight:'bold',
      color: '#FFF'
    })
    //  Crop the filler along its width, proportional to the amount of files
    //  loaded.
    this.load.on('progress', v => img.setCrop(0, 0, Math.ceil(v * w), h));
  }
}
