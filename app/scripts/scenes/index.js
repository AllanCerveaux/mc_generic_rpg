/*
 *  scenes `index` module
 *  =====================
 *
 *  Declares all present game scenes.
 *  Expose the required game scenes using this module.
 */

export {default as Author} from './author';
export {default as SplashScreen} from './splash-screen';
export {default as Village} from './village';
export {default as TitleScreen} from './title-screen';
