import * as config from '@/config';

export default class TitleScreen extends Phaser.Scene {
  /**
   *  My custom scene.
   *
   *  @extends Phaser.Scene
   */
  constructor() {
    super({key: 'TitleScreen'});
  }

  /**
   *  Called when this scene is initialized.
   *
   *  @protected
   *  @param {object} [data={}] - Initialization parameters.
   */
  init(/* data */) {
    this.panVelocityX = 25;
    this.panVelocityY = 18;
  }

  /**
   *  Used to declare game assets to be loaded using the loader plugin API.
   *
   *  @protected
   */
  preload() {
  }

  /**
   *  Responsible for setting up game objects on the screen.
   *
   *  @protected
   *  @param {object} [data={}] - Initialization parameters.
   */
  create(/* data */) {
    const bgStartX = Math.floor(Math.random()*256);
    const bgStartY = Math.floor(Math.random()*256);
    this.bgMap = this.physics.add.image(bgStartX, bgStartY, 'title-screen');
    this.bgMap.setVelocity(this.panVelocityX, this.panVelocityY);
    this.music = this.sound.add('celestial', {volume: 0.15, loop: true});
    this.bonus = this.sound.add('bonus', {volume: 0.15, loop: false});
    this.music.play();
    
    this.gameTitle();
    this.startGame();
    this.addKCode();
  }

  /**
   *  Handles updates to game logic, physics and game objects.
   *
   *  @protected
   *  @param {number} t - Current internal clock time.
   *  @param {number} dt - Time elapsed since last update.
   */
  update(/* t, dt */) {
    if (this.bgMap.x > config.width && this.panVelocityX > 0) {
      this.panVelocityX *= -1;
    }
    else if (this.bgMap.x < 0 && this.panVelocityX < 0) {
      this.panVelocityX *= -1;
    }
    
    if (this.bgMap.y > config.height && this.panVelocityY > 0) {
      this.panVelocityY *= -1;
    }
    else if (this.bgMap.y < 0 && this.panVelocityY < 0) {
      this.panVelocityY *= -1;
    }
    this.bgMap.setVelocity(this.panVelocityX, this.panVelocityY);

  }

  gameTitle(){
    this.title = this.add.image(config.width/2, config.height/2 - 200, 'game-title').setDepth(10).setScale(1.5);
    this.tweens.add({
      targets: this.title,
      y: config.height/2 - 70,
      duration: 2000,
      ease: 'Power2',
      yoyo: false,
    })
  }

  startGame(){
    this.startBtn = this.add.sprite(config.width/2, config.height/2, 'start-btn').setInteractive();
    
    this.startBtn.on('pointerdown', (pointer)=> {
      this.startBtn.setFrame(1);
      this.cameras.main.fadeOut(2000, 255, 255, 255, () => {
        this.cameras.main.on('camerafadeoutcomplete', () => {
          if(this.music.isPlaying || this.bonus.isPlaying){
            this.music.stop();
            this.bonus.stop();
          }
          this.scene.start('Village');
        }, this);
      })
    });

    this.startBtn.on('pointerup', (pointer) => {
      this.startBtn.setFrame(0);
    });
  }

  addKCode(){
    this.combo = this.input.keyboard.createCombo([38, 38, 40, 40, 37, 39, 37, 39, 66, 65, 13], { resetOnMatch: true });
    this.input.keyboard.on('keycombomatch', (event) => {
      console.log('Konami Code');
      this.music.pause()
      this.bonus.play();
    }, this);
  }

  /**
   *  Called after a scene is rendered. Handles rendenring post processing.
   *
   *  @protected
   */
  render() {
  }

  /**
   *  Called when a scene is about to shut down.
   *
   *  @protected
   */
  shutdown() {
  }

  /**
   *  Called when a scene is about to be destroyed (i.e.: removed from scene
   *  manager). All allocated resources that need clean up should be freed up
   *  here.
   *
   *  @protected
   */
  destroy() {
  }
}
